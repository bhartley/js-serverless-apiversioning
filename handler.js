'use strict';

const Controller = require('./controller')
const utils = require('./utils')
const defaultHeader = { 'Access-Control-Allow-Origin': '*' }

function test (event, context, callback) {
  const response = { headers: defaultHeader }
  const apiVersion = utils.getApiVersion(event.headers)
  const controller = new Controller(apiVersion)
  controller.do_thing()
    .then((msg) => {
      console.log('success')
      response.statusCode = 200
      response.body = JSON.stringify({ msg: msg })
      callback(null, response)
    })
    .catch((err) => {
      console.error(err)
      response.statusCode = 400
      response.body = JSON.stringify({ msg: 'something went wrong' })
      callback(null, response)
    })

}

module.exports = {
  test: test
}
