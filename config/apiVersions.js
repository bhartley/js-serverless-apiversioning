'use strict'

module.exports = [
    {
        name: 'alligator',
        version: 1
    },
    {
        name: 'buffalo',
        version: 2
    },
    {
        name: 'cobra',
        version: 3
    }
]
