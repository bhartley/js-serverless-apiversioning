'use strict'

module.exports = [
    function do_thing() {
        return new Promise((resolve, reject) => {
            const msg = 'doing that thing'
            console.log(msg)
            resolve(msg)
        })
    },
    function do_unchanging_trick() {
        return new Promise((resolve, reject) => {
            const msg = 'never gonna give you up'
            console.log(msg)
            resolve(msg)
        })
    }
]
