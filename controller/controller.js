'use strict'

const v1 = require('./controllerFunctionsV1')
const v2 = require('./controllerFunctionsV2')

class Controller {
  constructor(apiVersion) {
      if (apiVersion === 2) {
        this.apiVersion = 2
        for (let i = 0, len = v2.length; i < len; i++) {
            this[v2[i].name] = v2[i]
        }
      } else {
        this.apiVersion = 1
        for (let i = 0, len = v1.length; i < len; i++) {
            this[v1[i].name] = v1[i]
        }
      }
  }

}

module.exports = Controller
