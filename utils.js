'use strict'

const apiVersions = require('./config/apiVersions')

function getApiVersion (headers) {
  const lowerHeaders = keysToLowerCase(headers)
  const verHeader = lowerHeaders.apiversion

  // default to version 1, if apiversion header is present check config file for versio
  let version = 1
  if (verHeader) {
    for (let i = 0, len = apiVersions.length; i < len; i++) {
      if (apiVersions[i].name === verHeader) {
        version = apiVersions[i].version
        break
      }
    }    
  }
  return version
}

function keysToLowerCase (obj) {
  const keys = Object.keys(obj)
  let n = keys.length
  const tmp = {}

  while (n--) {
    let key = keys[n]; // "cache" it, for less lookups to the array
    tmp[key.toLowerCase()] = obj[key]
  }
  return (tmp)
}

module.exports = {
  getApiVersion: getApiVersion
}
